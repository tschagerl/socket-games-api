import GameCenterController from '../src/GameCenterController';
import GameController from '../src/GameController';

export {
    GameCenterController,
    GameController
}
