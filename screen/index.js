import GameCenterScreen from '../src/GameCenterScreen';
import GameScreen from '../src/GameScreen';

export {
    GameCenterScreen,
    GameScreen
}
