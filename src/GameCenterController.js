import Controller from './base/Controller';

export default class GameCenterController {

    constructor(onConnect, onError, screenId) {
        if (top !== self) {
            throw 'Use GameController inside iframe for games.';
        }
        this._screenId = screenId;
        this._api = new Controller({
            url: null,
            screenId,
            onConnect,
            onError,
            useWebRTC: false
        });

        // TODO : use postmessaging api (and allow cross-origin?)
        // TODO : (e.g. https://www.npmjs.com/package/jschannel)
        window.addEventListener('message', event => {
            if (event.origin !== location.origin) {
                return;
            }
            if (event.data.key === 'socket-games:request-screen-id') {
                event.source.postMessage({
                    key: 'socket-games:screen-id',
                    url: location.origin,
                    screenId: this._screenId
                }, event.origin);
            }
        });
    }

    on() {
        this._api.on(...arguments);
        return this;
    }

    emit() {
        this._api.emit(...arguments);
        return this;
    }

}
