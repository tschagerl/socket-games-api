import Screen from './base/Screen';

export default class GameCenterScreen {

    constructor(onConnect, onError) {
        if (top !== self) {
            throw 'Use GameScreen inside iframe for games.';
        }
        this._screenId = null;
        try {
            this._screenId = localStorage.getItem('socket-games:screen-id');
        } catch (e) {
            console.error('GameCenterScreen - unable to read screenId from localstorage', e);
        }
        this._api = new Screen({
            url: null,
            screenId: this._screenId,
            onConnect: data => {
                this._screenId = data.screenId;
                try {
                    localStorage.setItem('socket-games:screen-id', this._screenId);
                } catch (e) {
                    console.error('GameCenterScreen - unable to store screenId into localstorage', e);
                }
                onConnect(data);
            },
            onError,
            useWebRTC: false
        });

        // TODO : use postmessaging api (and allow cross-origin?)
        // TODO : (e.g. https://www.npmjs.com/package/jschannel)
        window.addEventListener('message', event => {
            if (event.origin !== location.origin) {
                return;
            }
            if (event.data.key === 'socket-games:request-screen-id') {
                event.source.postMessage({
                    key: 'socket-games:screen-id',
                    url: location.origin,
                    screenId: this._screenId
                }, event.origin);
            }
        });
    }

    on() {
        this._api.on(...arguments);
        return this;
    }

    emit() {
        this._api.emit(...arguments);
        return this;
    }

    broadcast() {
        this._api.emit(...arguments);
        return this;
    }

}
