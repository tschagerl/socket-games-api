import Screen from './base/Screen';

export default class GameScreen {

    constructor(onConnect, onError, useWebRTC = true) {
        if (top === self) {
            throw 'Use GameCenterScreen for stand-alone.';
        }
        this._useWebRTC = useWebRTC;

        // TODO : use postmessaging api (and allow cross-origin?)
        // TODO : (e.g. https://www.npmjs.com/package/jschannel)
        window.addEventListener('message', event => {
            if (event.origin !== location.origin) {
                return;
            }
            if (event.data.key === 'socket-games:screen-id') {
                this._setupApi(event.data.url, event.data.screenId, onConnect, onError);
            }
        });
        top.postMessage({
            key: 'socket-games:request-screen-id'
        }, location.origin);
    }

    on() {
        this._api.on(...arguments);
        return this;
    }

    emit() {
        this._api.emit(...arguments);
        return this;
    }

    broadcast() {
        this._api.emit(...arguments);
        return this;
    }

    /*
     * PRIVATES
     */

    _setupApi(url, screenId, onConnect, onError) {
        this._api = new Screen({
            url,
            screenId: screenId + '-game',
            onConnect,
            onError,
            useWebRTC: this._useWebRTC
        });
    }

}
