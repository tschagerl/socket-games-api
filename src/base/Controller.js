import io from 'socket.io-client';

import Channel from '../webrtc/Channel';
import WebRTCConnector from '../webrtc/WebRTCConnector';

const defaultOptions = {
    url: null,
    screenId: null,
    onConnect: null,
    onError: null,
    useWebRTC: false
};

export default class Controller {

    constructor(options) {
        this._options = Object.assign({}, defaultOptions, options);
        this._socket = io(this._options.url);
        this._callbacks = {
            'screen-disconnected': []
        };
        this._webRTCConnection = null;

        this._socket.on('connect_error', error => {
            if (this._options.onError) {
                this._options.onError(error);
            }
        });

        this._socket.on('connect_timeout', timeout => {
            if (this._options.onError) {
                this._options.onError(timeout);
            }
        });

        this._socket.emit('join-screen', this._options.screenId, data => {
            if (data.error) {
                if (this._options.onError) {
                    this._options.onError(data.error);
                }
                return;
            }

            this._socket.on('screen-disconnected', () => {
                this._callbacks['screen-disconnected'].forEach(callback => callback());
            });

            this._socket.on('screen-event', (eventWrapper, socketCallback) => {
                if (this._webRTCConnection) {
                    return;
                }
                const {event, data} = eventWrapper;
                if (!this._callbacks[event]) {
                    return;
                }
                this._callbacks[event].forEach(callback => callback(data, socketCallback));
            });

            if (this._options.onConnect) {
                this._options.onConnect(data);
            }

            this._upgradeWebRTC();
        });
    }

    on(event, callback) {
        if (typeof callback !== 'function') {
            return this;
        }
        if (!this._callbacks[event]) {
            this._callbacks[event] = [];
        }
        this._callbacks[event].push(callback);
        return this;
    }

    emit(event, data, callback) {
        if (this._webRTCConnection) {
            this._webRTCConnection.emit(event, data, callback);
            return this;
        }
        let wrapper = {
            event,
            data
        };
        this._socket.emit('controller-event', wrapper, callback);
        return this;
    }

    _upgradeWebRTC() {
        if (!this._options.useWebRTC) {
            return;
        }
        const channel = new Channel((event, data) => {
            this.emit('webrtc-setup', {
                event,
                data
            });
        });
        this.on('webrtc-setup', (eventWrapper) => {
            const {event, data} = eventWrapper;
            channel.trigger(event, data);
        });
        const webRTCConnection = new WebRTCConnector(channel);
        webRTCConnection.on('connect', () => this._webRTCConnection = webRTCConnection);
        webRTCConnection.on('connect_error', () => this._webRTCConnection = null);
        webRTCConnection.on('disconnect', () => this._webRTCConnection = null);
        webRTCConnection.on('message', (event, data, eventCallback) => {
            if (!this._callbacks[event]) {
                return;
            }
            this._callbacks[event].forEach(callback => callback(data, eventCallback));
        });
        webRTCConnection.connect();
    }

}
