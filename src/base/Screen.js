import io from 'socket.io-client';

import Channel from '../webrtc/Channel';
import WebRTCConnector from '../webrtc/WebRTCConnector';

const defaultOptions = {
    url: null,
    screenId: null,
    onConnect: null,
    onError: null,
    useWebRTC: false
};

export default class Screen {

    constructor(options) {
        this._options = Object.assign({}, defaultOptions, options);
        this._socket = io(this._options.url);
        this._callbacks = {
            'controller-connected': [],
            'controller-disconnected': []
        };
        this._webRTCConnections = {};

        this._socket.on('connect_error', error => {
            if (this._options.onError) {
                this._options.onError(error);
            }
        });

        this._socket.on('connect_timeout', timeout => {
            if (this._options.onError) {
                this._options.onError(timeout);
            }
        });

        this._socket.emit('create-screen', this._options.screenId, data => {
            if (data.error) {
                if (this._options.onError) {
                    this._options.onError(data.error);
                }
                return;
            }

            // a new controller connected to this screen
            this._socket.on('controller-connected', (controllerId, callback) => {
                this._callbacks['controller-connected'].forEach(callback => callback(controllerId));
                this._upgradeWebRTC(controllerId);
                callback();
            });

            // a controller disconnected from this screen
            this._socket.on('controller-disconnected', controllerId => {
                this._callbacks['controller-disconnected'].forEach(callback => callback(controllerId));
                delete this._webRTCConnections[controllerId];
            });

            // controller wraps generic events into "controller-event"
            this._socket.on('controller-event', (eventWrapper, socketCallback) => {
                const {event, data, controllerId} = eventWrapper;
                if (this._options.useWebRTC && this._webRTCConnections[controllerId]) {
                    return;
                }
                if (!this._callbacks[event]) {
                    return;
                }
                this._callbacks[event].forEach(callback => callback(data, controllerId, socketCallback));
            });

            if (this._options.onConnect) {
                this._options.onConnect(data);
            }
        });
    }

    on(event, callback) {
        if (typeof callback !== 'function') {
            return this;
        }
        if (!this._callbacks[event]) {
            this._callbacks[event] = [];
        }
        this._callbacks[event].push(callback);
        return this;
    }

    emit(event, data, clientId, callback) {
        if (typeof clientId === 'function' || typeof clientId === 'undefined') {
            this.broadcast(event, data, clientId);
            return;
        }
        let wrapper = {
            event,
            data
        };
        if (this._options.useWebRTC && this._webRTCConnections[clientId]) {
            this._webRTCConnections[clientId].emit(event, data, callback);
            return;
        }
        wrapper.clientId = clientId;
        this._socket.emit('screen-event', wrapper, callback);
        return this;
    }

    broadcast(event, data, callback) {
        let wrapper = {
            event,
            data
        };
        if (this._options.useWebRTC) {
            for (let controllerId in this._webRTCConnections) {
                this._webRTCConnections[controllerId].emit(event, data, callback);
            }
        }
        this._socket.emit('screen-event', wrapper, callback);
        return this;
    }

    _upgradeWebRTC(controllerId) {
        if (!this._options.useWebRTC) {
            return;
        }
        const channel = new Channel((event, data) => {
            this.emit('webrtc-setup', {
                event,
                data
            }, controllerId);
        });
        this.on('webrtc-setup', (eventWrapper, eventControllerId) => {
            const {event, data} = eventWrapper;
            if (eventControllerId !== controllerId) {
                return;
            }
            channel.trigger(event, data);
        });
        const webRTCConnection = new WebRTCConnector(channel);
        webRTCConnection.on('connect', () => this._webRTCConnections[controllerId] = webRTCConnection);
        webRTCConnection.on('connect_error', () => delete this._webRTCConnections[controllerId]);
        webRTCConnection.on('disconnect', () => delete this._webRTCConnections[controllerId]);
        webRTCConnection.on('message', (event, data, eventCallback) => {
            if (!this._callbacks[event]) {
                return;
            }
            this._callbacks[event].forEach(callback => callback(data, controllerId, eventCallback));
        });
    }

}
