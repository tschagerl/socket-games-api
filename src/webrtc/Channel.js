export default class Channel {

    /**
     *
     * @param emit function - the connector uses this function to send data to the connector on the other side
     */
    constructor(emit) {
        this.emit = emit;

        this._callbacks = {
        };
    }

    /**
     *
     * @param event string
     * @param callback function
     * @returns {Channel}
     */
    on(event, callback) {
        if (typeof callback !== 'function') {
            return this;
        }
        this._callbacks[event] = callback;
        return this;
    }

    /**
     * use this function to send message from one connector to the other one
     *
     * @param event string
     * @param data mixed
     */
    trigger(event, data) {
        if (this._callbacks[event]) {
            this._callbacks[event](data);
        }
    }

};
