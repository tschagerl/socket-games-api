export const pcConfig = {
    iceServers: [{
        urls: ['stun:stun.l.google.com:19302']
    }, {
        urls: ['stun:stun1.l.google.com:19302']
    }, {
        urls: ['stun:stun2.l.google.com:19302']
    }, {
        urls: ['stun:stun3.l.google.com:19302']
    }, {
        urls: ['stun:stun4.l.google.com:19302']
    }, {
        urls: ['stun:stunserver.org']
    }],
    iceTransportPolicy: 'all',
    rtcpMuxPolicy: 'negotiate'
};

export const pcConstraints = {
    optional: [{'googIPv6': true}]
};

export const STATUS = {
    DISCONNECTED: 0,
    NO_SUPPORT: 1,
    CONNECTING_OFFER: 2,
    CONNECTING_ANSWER: 3,
    CONNECTION_FAILED: 4,
    CONNECTION_CLOSED: 5,
    SEND_OPEN: 6,
    RECEIVE_OPEN: 7,
    SEND_RECEIVE_OPEN: 8,
};

export default class WebRTCConnector {

    /**
     *
     * @param channel {Channel} - the channeling service used to establish the connection.
     */
    constructor(channel) {
        this._channel = channel;
        this._callbacks = {
            'connect': [],
            'connect_error': [],
            'disconnect': [],
            'message': []
        };
        this._messageCallbacks = [];


        this._status = STATUS.DISCONNECTED;
        this._peerConnection = null;
        this._sendChannel = null;
        this._receiveChannel = null;

        if (!RTCPeerConnection) {
            this._status = STATUS.NO_SUPPORT;
            return;
        }

        this._peerConnection = new RTCPeerConnection(pcConfig, pcConstraints);
        this._peerConnection.onicecandidate = event => this._onIceCandidate(event);
        this._peerConnection.ondatachannel = event => this._onReceiveChannel(event);
        this._sendChannel = this._peerConnection.createDataChannel('sendChannel');
        this._sendChannel.onopen = () => this._onChannelStateChange();
        this._sendChannel.onclose = () => this._onChannelStateChange();

        this._channel.on('offer', description => this._onOfferReceived(description));
        this._channel.on('answer', description => this._onAnswerReceived(description));
        this._channel.on('ice-candidate', data => this._onRemoteIceCandidate(data));
    }

    /**
     * try to establish connection to the other side. only call this func on one side.
     *
     * @returns void
     */
    connect() {
        if (this._status !== STATUS.DISCONNECTED) {
            this._trigger('connect_error', ['cant connect. status not disconnected.']);
            return;
        }
        this._status = STATUS.CONNECTING_OFFER;

        this._peerConnection.createOffer()
            .then(description => this._onCreateOfferSuccess(description))
            .then(() => this._onSetLocalDescriptionSuccess())
            .catch(error => this._onConnectionError(error));
    }

    on(event, callback) {
        if (typeof callback !== 'function') {
            return this;
        }
        if (!this._callbacks[event]) {
            this._callbacks[event] = [];
        }
        this._callbacks[event].push(callback);
        return this;
    }

    /**
     * send event to paired connector
     *
     * @param event string
     * @param data [mixed]
     * @param callback [function]
     * @returns {WebRTCConnector}
     */
    emit(event, data, callback) {
        if (this._status === STATUS.CONNECTION_FAILED || this._status === STATUS.CONNECTION_CLOSED) {
            this._trigger('connect_error', ['unable to emit. no connection available.']);
            return this;
        }
        let wrapper = {
            event,
            data
        };
        if (typeof callback === 'function') {
            wrapper.callbackId = this._messageCallbacks.length;
            this._messageCallbacks.push(callback);
        }
        this._sendChannel.send(JSON.stringify(wrapper));
        return this;
    }

    _trigger(event, callbackArgs) {
        if (!this._callbacks[event]) {
            return;
        }
        this._callbacks[event].forEach(callback => callback(...callbackArgs));
    }

    _onCreateOfferSuccess(description) {
        if (this._status !== STATUS.CONNECTING_OFFER) {
            return;
        }
        this._channel.emit('offer', description);
        return this._peerConnection.setLocalDescription(description);
    }

    _onSetLocalDescriptionSuccess() {
    }

    _onIceCandidate(event) {
        if (this._status !== STATUS.CONNECTING_OFFER && this._status !== STATUS.CONNECTING_ANSWER) {
            return;
        }
        if (!event.candidate) {
            return;
        }
        this._channel.emit('ice-candidate', {
            type: 'candidate',
            label: event.candidate.sdpMLineIndex,
            id: event.candidate.sdpMid,
            candidate: event.candidate.candidate
        });
    }

    _onRemoteIceCandidate(data) {
        if (this._status !== STATUS.CONNECTING_OFFER) {
            return;
        }
        const candidate = new RTCIceCandidate({
            sdpMLineIndex: data.label,
            candidate: data.candidate
        });
        this._peerConnection.addIceCandidate(candidate);
    }

    _onOfferReceived(description) {
        if (this._status !== STATUS.DISCONNECTED) {
            return;
        }
        this._status = STATUS.CONNECTING_OFFER;
        this._peerConnection.setRemoteDescription(new RTCSessionDescription(description))
            .then(() => this._peerConnection.createAnswer())
            .then(description => this._onCreateAnswerSuccess(description))
            .then(() => this._onSetLocalDescriptionSuccess())
            .catch(error => this._onConnectionError(error));
    }

    _onCreateAnswerSuccess(description) {
        this._channel.emit('answer', description);
        return this._peerConnection.setLocalDescription(description);
    }

    _onAnswerReceived(description) {
        if (this._status !== STATUS.CONNECTING_OFFER) {
            return;
        }
        this._peerConnection.setRemoteDescription(new RTCSessionDescription(description))
            .then(() => this._onSetRemoteDescriptionSuccess())
            .catch(error => this._onConnectionError(error));
    }

    _onSetRemoteDescriptionSuccess() {
    }

    _onReceiveChannel(event) {
        this._receiveChannel = event.channel;
        this._receiveChannel.onmessage = event => this._onReceiveMessage(event);
        this._receiveChannel.onopen = () => this._onChannelStateChange();
        this._receiveChannel.onclose = () => this._onChannelStateChange();
    }

    _onChannelStateChange() {
        let sendChannelOpen = false,
            receiveChannelOpen = false;
        switch (this._sendChannel.readyState) {
            case 'open':
                this._status = STATUS.SEND_OPEN;
                sendChannelOpen = true;
                break;
            case 'closed':
                this._status = STATUS.CONNECTION_CLOSED;
                return;
            default:
        }
        if (!this._receiveChannel) {
            return;
        }
        switch (this._receiveChannel.readyState) {
            case 'open':
                this._status = STATUS.RECEIVE_OPEN;
                receiveChannelOpen = true;
                break;
            case 'closed':
                this._status = STATUS.CONNECTION_CLOSED;
                return;
            default:
        }
        if (sendChannelOpen && receiveChannelOpen) {
            this._status = STATUS.SEND_RECEIVE_OPEN;
            this._trigger('connect', []);
        }
    }

    _onConnectionError(error) {
        console.error('socket-games-api/WebRTCConnector : unable to establish WebRTC', error);
        this._status = STATUS.CONNECTION_FAILED;
        this._trigger('connect_error', [error]);
    }

    _onReceiveMessage(messageEvent) {
        const {event, data, callbackId} = JSON.parse(messageEvent.data);
        if (!event && typeof callbackId === 'number') {
            this._messageCallbacks[callbackId](data);
            return;
        }
        const callbackArgs = [
            event,
            data
        ];
        if (typeof callbackId === 'number') {
            callbackArgs.push(data => {
                const wrapper = {
                    data,
                    callbackId
                };
                this._sendChannel.send(JSON.stringify(wrapper));
            });
        }
        this._trigger('message', callbackArgs);
    }

};
